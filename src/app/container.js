import React, {Component} from 'react';
import { Link } from 'react-router';
import { Nav } from './nav';
import { FacebookLogin } from './facebook-login';
import { EventGrid } from './event-grid';

export class Container extends Component {
  constructor(props) {
    super(props);
  }
  
  render() {
    return (
      <div>
        <Nav />
        <div className="container">
          <FacebookLogin />
          { React.cloneElement(this.props.children, this.props) }
          <div className="fixed-action-btn" >
            <Link to="/events/new" className="btn-floating btn-large yellow darken-1">
              <i className="material-icons">add</i>
            </Link>
          </div>
        </div>
      </div>
    );
  }
}