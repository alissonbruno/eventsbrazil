import React, { Component } from 'react';
import * as axios from 'axios';

export class LocationFields extends Component {
  constructor(props) {
    super(props);
    this.handleChange = this.handleChange.bind(this);
    this.values = this.values.bind(this);
    this.findLocation = this.findLocation.bind(this);
    this.state = {
      zipcode: '',
      local: '',
      street: '',
      neighbor: '',
      city: '',
      state: ''
    }
  }


  handleChange(event) {
    const element = event.target;
    let aux = {};
    aux[element.name] = element.value;
    this.setState(aux);
  }

  values() {
    return Object.assign({}, this.state);
  }

  
  findLocation(event) {
    let zipcode = event.target.value;
    this.setState({zipcode: zipcode});
    if(zipcode.replace('-', '').length < 8) return;
    axios.get(`https://viacep.com.br/ws/${zipcode}/json/`).then((response) =>{
      let { data } = response;
      this.setState({
        zipcode: data.cep,
        street: data.logradouro,
        neighbor: data.bairro,
        city: data.localidade,
        state: data.uf
      })
    }).catch((err) =>{
      console.error("Error:", err);
    })
  }

  render() {
    return (
      <div>
        <h4>Endereço</h4>
        <div className="input-field col s12">
          <input placeholder="Local" 
            id="local"
            name="local" 
            type="text"
            value={ this.state.local }
            onChange={ this.handleChange }
          />
          <label htmlFor="local" className="active">Local</label>
        </div>
        <div className="input-field col s12">
          <input placeholder="CEP" 
            id="zipcode"
            name="zipcode" 
            type="text"
            value={ this.state.zipcode }
            onChange={ this.findLocation }
          />
          <label htmlFor="zipcode" className="active">CEP</label>
        </div>
        <div className="input-field col s12">
          <input placeholder="Logradouro" 
            id="street"
            name="street"  
            type="text"
            value={ this.state.street }
            onChange={ this.handleChange }
          />
          <label htmlFor="street" className="active">Logradouro</label>
        </div>
        <div className="input-field col s12">
          <input placeholder="Bairro" 
            id="neighbor"
            name="neighbor"  
            type="text"
            value={ this.state.neighbor }
            onChange={ this.handleChange }
          />
          <label htmlFor="neighbor" className="active">Bairro</label>
        </div>
        <div className="input-field col s8">
          <input placeholder="Cidade" 
            id="city"
            name="city"  
            type="text"
            value={ this.state.city }
            onChange={ this.handleChange }
          />
          <label htmlFor="city" className="active">Cidade</label>
        </div>
        <div className="input-field col s4">
          <input placeholder="Estado" 
            id="state"
            name="state"  
            type="text"
            value={ this.state.state }
            onChange={ this.handleChange }
          />
          <label htmlFor="state" className="active">UF</label>
        </div>
      </div>
    );
  }
}