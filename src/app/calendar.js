import React, { Component } from 'react';
import moment from 'moment';

export class Calendar extends Component {
  constructor(props) {
    super(props);
    this.state = {
      month: null,
      day: null
    }
  }

  componentDidMount() {
    const { date } = this.props;
    if(date) {
      this.setState({
        month: moment(date).format('MMM'),
        day: moment(date).format('DD')
      });
    }
  }

  render() {
    const { month, day } = this.state;
    return (
      <div className="date">
        <div className="month">{ month }</div>
        <div className="day">{ day }</div>
      </div>
    );
  }
}
