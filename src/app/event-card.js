import React, { Component } from 'react';
import { Calendar } from './calendar';
import moment from 'moment';

export class EventCard extends Component {
  render() {
    const { event } = this.props;
    return (
      <div className="col s12 m6 l3">
        <div className="card event-card ">
          <Calendar date={ event.date } />
          <div className="card-image">
            <img src={ event.thumb } />
          </div>
          <div className="card-event-title">
            <span>{ event.title }</span>
          </div>
          <div className="location">
            <div className="info-hour">
              <i className="material-icons">query_builder</i>
              <span>{ moment(event.date).format('h:mmA') }</span>
            </div>
            <div className="info-place">
              <i className="material-icons">place</i>
              <span>{ event.location }</span>
            </div>
          </div>
        </div>
      </div>
    );
  }
}