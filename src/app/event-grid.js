import React, { Component } from 'react';
import { Link } from 'react-router';
import { EventCard } from './event-card';
import { Event } from './services/event/event-api';


export class EventGrid extends Component {
  constructor(props) {
    super(props);
    this.state = {
      events: []
    }
  }

  componentDidMount() {
    Event.public.all((response) =>{
      this.setState({
        events: response.data
      });
    },(error)=>{
      console.log("Something went wrong!");
    });
  }

  render() {
    const { events } = this.state;
    return (
      <div className="row">
        {events.map((event, index) => {
          return(
            <Link to={`events/${event.id}`} key={index}>
              <EventCard event={ event }/>
            </Link>      
          );
        }, this)}
      </div>
    );
  }
}