import React, {Component} from 'react';
import { Session, API } from './services/service';
import * as Cookies  from 'js-cookie';

export class FacebookLogin extends Component {
  constructor(props) {
    super(props);
    this.state = {
      logged: false,
      disabled: true
    }
  }

  componentWillMount() {
    let self = this;
    window.fbAsyncInit = function() {
      FB.init({
        appId      : '169937926671851',
        xfbml      : true,
        version    : 'v2.3'
      });
      self.setState({
        disabled: false
      });
    };
  }

  componentDidMount() {
    (function(d, s, id){
      var js, fjs = d.getElementsByTagName(s)[0];
      if (d.getElementById(id)) {return;}
      js = d.createElement(s); js.id = id;
      js.src = "//connect.facebook.net/en_US/sdk.js";
      fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
  }

  getInfo() {
    FB.api('/me?fields=id,email,name', function(response) {
      let params = Object.assign({provider: 'facebook'}, response);
      Session.create(params, (res) => {
        Cookies.set('token', res.data.token);
        self.setState({
          logged: true
        });
      }, (error) => {

      });
      
    });
  }

  login() {
    let self = this;
    FB.getLoginStatus(function(response) {
      if (response.status === 'connected') {
        console.log(response);
        self.getInfo();
      }
      else {
        FB.login(function(response, a, b, c) {
          self.getInfo();
        }, { scope: "email,public_profile", return_scopes: true });
      }
    });
  }

  logout() {
    let self = this;
    FB.logout(function(response) {
      self.setState({
        logged: false
      })
    });
  }

  render() {
    let { logged, disabled } = this.state;
    var button;
    if(logged) {
      button = <button className="btn" onClick={ this.logout.bind(this) }>Log out</button>;
    } else {
      button = <button className="btn blue" disabled={ disabled }onClick={ this.login.bind(this) }>Entrar com facebook</button>;
    }
    return button;
  }
}