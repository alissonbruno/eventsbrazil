import * as axios from 'axios';

const BASE_URL = 'http://localhost:3002'

export const Session = {
  create: function(params, success, error) {
    axios.post(`${BASE_URL}/login.json`, params).then(success).catch(error);
  }
}