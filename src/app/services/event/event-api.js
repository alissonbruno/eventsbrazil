import { PublicApi } from './public-api';
import { PrivateApi } from './private-api';

export const Event = {
  public: new PublicApi(),
  private: new PrivateApi()
}