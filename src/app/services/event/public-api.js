import * as axios from 'axios';

export class PublicApi {
  constructor() {
    this.http = axios.create({
      baseURL: 'http://localhost:3002'
    });
  }

  all(success, error) {
    this.http.get('/events.json').then(success).catch(error);
  }
  
  find(id, success, error) {
    this.http.get(`/events/${id}.json`).then(success).catch(error);
  }
}