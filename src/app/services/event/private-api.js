import * as axios from 'axios';
import * as Cookies from 'js-cookie';

export class PrivateApi {
  constructor() {
    this.http = axios.create({
      baseURL: 'http://localhost:3002',
      headers: {
        Authorization: `Bearer ${Cookies.get('token')}`
      }
    });
  }

  save(model, success, error) {
    this.http.post('/events.json', model).then(success).catch(error);
  }

  signRequest(success, error) {
    this.http.get('/sign-s3.json').then(success).catch(error);
  }

  uploadFile(url, data, config) {
    return axios.post(url, data, config);
  }
}