import React, {Component} from 'react';
import { Event } from './services/event/event-api';

export class FileS3Upload extends Component {
  constructor(props) {
    super(props);
    this.value = this.props.value;
    this.onChange = this.onChange.bind(this);
    this.formData = this.formData.bind(this);
    this.uploadFile = this.uploadFile.bind(this);
    this.progress = this.progress.bind(this);
  }

  progress(event) {
    let completed = parseInt(event.loaded / event.total * 100, 10);
    let { progressBar } = this.refs;
    progressBar.style.width = `${completed}%`
  }


  formData(file, signedRequest) {
    let data = new FormData();
    Object.keys(signedRequest).forEach(function(key) {
      data.append(key, signedRequest[key]);
    });
    data.append('file', file);
    return data;
  }

  uploadFile(file, signedRequest, url) {
    let self = this;
    let data = self.formData(file, signedRequest)
    Event.private.uploadFile(url, data, { progress: self.progress })
      .then((response) =>{
        let url = response.request.responseXML.getElementsByTagName('Location')[0].innerHTML;
        self.value(unescape(url));
      })
      .catch((error) => {
        console.error('PostPhoto', error);
      });
  }

  onChange(e) {
    let self = this;
    const file = e.target.files[0];
    if(file == null) return alert('No file selected.');
    Event.private.signRequest((response) => {
      self.uploadFile(file, response.data.data, response.data.url);
    }, (error) => {
      console.error('SignUrl', error);
    });
  }
 
  render() {
    return (
      <div className="file-field input-field">
        <div className="btn waves-effect waves-light btn-primary">
          <span>Banner</span>
          <input placeholder="Imagem" 
            id="image"
            name="image" 
            type="file"
            onChange={ this.onChange }
          />
        </div>
        <div className="file-path-wrapper">
          <div className="progress yellow lighten-3">
            <div className="determinate yellow darken-1" ref="progressBar"></div>
          </div>
        </div>
      </div>
    );
  }
}