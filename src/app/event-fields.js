import React, { Component } from 'react';
import { FileS3Upload } from './file-s3-upload';

export class EventFields extends Component {
  constructor(props) {
    super(props);
    this.handleChange = this.handleChange.bind(this);
    this.values = this.values.bind(this);
    this.state = {
      title: '',
      description: '',
      image_url: ''
    }
  }

  handleChange(event) {
    const element = event.target;
    let aux = {};
    aux[element.name] = element.value;
    this.setState(aux);
  }

  updateImage(image_url) {
    this.setState({
      image_url: image_url
    })
  }

  values() {
    return Object.assign({}, this.state);
  }

  render() {
    return (
      <div>
        <h4>Endereço</h4>
        
        <div className="col s12 m12 l6">
          <div className="input-field col s12">
            <FileS3Upload value={ this.updateImage.bind(this) }/>
          </div>
          <div className="input-field col s12">
            <input placeholder="Título" 
              id="title"
              name="title" 
              type="text"
              value={ this.state.title }
              onChange={ this.handleChange }
            />
            <label htmlFor="title" className="active">Título</label>
          </div>
          <div className="input-field col s12">
            <textarea placeholder="Descrição" 
              className="materialize-textarea"
              id="description"
              name="description" 
              type="text"
              value={ this.state.description }
              onChange={ this.handleChange }
            />
            <label htmlFor="description" className="active">Descrição</label>
          </div>
        </div>
      </div>
    );
  }
}