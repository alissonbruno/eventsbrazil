import React, { Component } from 'react';
import { Link } from 'react-router';

export class Nav extends Component {
  render() {
    return (
      <div>
        <nav className="nav">
          <div className="container">
            <Link to="/" className="brand-logo">Logo</Link>
            <ul id="nav-mobile" className="right hide-on-med-and-down">
              <li><Link to="/about"><i className="material-icons">info_outline</i></Link></li>
            </ul>
          </div>
        </nav>
      </div>
    );
  }
}