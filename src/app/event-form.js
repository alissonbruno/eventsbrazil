import React, { Component } from 'react';
import { LocationFields } from './location-fields';
import { EventFields } from './event-fields';
import { Event } from './services/event/event-api';

export class EventForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      location: {},
      event: {}
    };
  }

  onSubmit(e) {
    e.preventDefault();
    let { history } = this.props;
    const { event, location } = this.refs; 
    let eventModel = Object.assign(event.values(), location.values());
    Event.private.save(eventModel, (response) => {
      history.push('/');
    },(error) => {
      console.log("Something went wrong!!!", error);
    });
  }

  render() {
    const { local, city } = this.state.location;
    return (
      <div>
        <div className="row">
          <form onSubmit={ this.onSubmit.bind(this) }>
            <div className="row">
              <div className="col s12 m12 l12 form-card">
                <EventFields ref="event" />
              </div>
              <div className="col s12 m6 l4 form-card" >
                <div className="form-card">
                  <LocationFields ref="location" />
                </div>
              </div>
            </div>
            <input type="submit" value="Cadastrar" />
          </form>
        </div>
      </div>
    );
  }
}