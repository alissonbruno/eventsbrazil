import React, { Component } from 'react';
import { Event } from './services/event/event-api';

export class EventSingle extends Component {
  constructor(props) {
    super(props);
    this.state = {
      event: {
        id: null,
        title: null,
        description: null,
        image: null
      }
    }
  }

  componentDidMount() {
    const { id } = this.props.params;
    if(id) {
      Event.public.find(id, (response) => {
        this.setState({
          event: response.data
        });
      }, (error) => {
        console.log('FindEvent', error);
      });
    }
  }

  render() {
    const { event } = this.state;
    return (
      <div className="row">
        <div className="col s12 m12">
          <div className="card single-event">
            <div className="card-image">
              <img src={event.image} />
              <span className="card-title">{event.title}</span>
            </div>
            <div className="card-content">
              <p>{event.description}</p>
            </div>
            <div className="card-action">
              <a href="#">This is a link</a>
            </div>
          </div>
        </div>
      </div>
    );
  }
}