import React from 'react';
import ReactDOM from 'react-dom';
import { Router, Route, IndexRoute, browserHistory } from 'react-router';

import { Container } from './app/container';
import { EventForm } from './app/event-form';
import { About } from './app/about';
import { EventGrid } from './app/event-grid';
import { EventSingle } from './app/event-single';

import './index.scss';


ReactDOM.render(
  <Router history={ browserHistory }>
    <Route path="/" component={ Container } >
      <IndexRoute component={ EventGrid } />
      <Route path="/events/new" component={ EventForm } />
      <Route path="/events/:id" component={ EventSingle }/>
      <Route path="/about" component={ About }/>
    </Route>
  </Router>,
  document.getElementById('root')
);